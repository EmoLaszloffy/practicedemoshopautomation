package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
@Epic("Login User Home page test")
@Feature("User can log in and perform operation on Home page ")
public class LoginUserHomePageTest {
    Page page = new Page();
    Header header = new Header();
    ModalPage modal = new ModalPage();
    ProductCards productList = new ProductCards();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickOnTheResetButton();
    }

    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("valid user can navigate to Cart page from Home page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story(" valid user cart page navigation from home page")
    public void valid_user_navigate_to_cart_page(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(),"Your cart","expected to be on the Cart page.");
    }
    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("Valid user can navigate to Home page from Cart page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("navigation to home page from cart page for valid user")
    public void valid_user_can_navigate_to_Home_page_from_Cart_Page(ValidAccount account){
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(),"Products","Expected to be on Products page");
    }
    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("valid user can navigate to Home page from Wishlist ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("home page navigation from wishlist page for valid user")
    public void valid_user_can_navigate_to_Home_Page_from_Wishlist_Page(ValidAccount account){
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        header.clickOnTheWishlistIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Products page.");
    }
    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("valid user can sort product alphabetically ASC from A to Z")
    @Severity(SeverityLevel.MINOR)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("valid user can sort products alphabetically from A to Z")
    public void valid_user_can_sort_products_alphabetically_ASC(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheAZSortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();
        assertEquals(firstProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
    }

    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("valid user can sort product alphabetically DESC from Z to A")
    @Severity(SeverityLevel.MINOR)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("valid user can sort products alphabetically from Z to A")
    public void valid_user_can_sort_products_alphabetically_DESC(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheZASortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();
        assertEquals(firstProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
    }

    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("Valid user can sort product by price ASC from low to high")
    @Severity(SeverityLevel.MINOR)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("valid user can sort products by price from low to high")
    public void valid_user_can_sort_products_by_price_from_low_to_high_ASC(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();
        assertEquals(firstProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
    }

    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("valid user can sort product by price DESC from high to low")
    @Severity(SeverityLevel.MINOR)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("valid user can sort products by price from high to low")
    public void when_sorting_products_by_price_high_to_low_valid_user_can_sort_products_by_price_DESC(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceHiLo();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();
        assertEquals(firstProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
    }

    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("valid user can add one product from product cart to wishlist")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("valid user can add product to wishlist")
    public void valid_user_can_add_one_product_to_wishlist_from_product_cart(ValidAccount account){
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        Product product = new Product("5");
        product.clickOnTheWishlistHartIcon();
        assertTrue(header.isWishlistBadgeVisible());
        String badgeValue = header.getWishlistBadgeValue();
        assertEquals(badgeValue, "1", "After adding one products to wishlist, badge shows 1");
    }
}
