package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

@Epic("Address Information Tests  ")
@Feature(" User can navigate to Your Information page and perform operations")

public class AddressInformationTest {

    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    CheckoutPage checkoutPage = new CheckoutPage();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickOnTheResetButton();
        header.clickOnTheShoppingBagIcon();
    }

    @Test(description = "adding one product to cart user can click on checkout button and complete delivery information")
    @Description(" after adding one product to cart user can navigate to checkout page and complete delivery info ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("navigation to to checkout page and completing delivery information after adding one product to cart page ")
    public void user_can_click_on_Checkout_button_and_complete_delivery_info() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        checkoutPage.typeInFirstname(" Sherlock");
        checkoutPage.typeInLastname(" Holmes");
        checkoutPage.typeInAddress(" Backer Street");
        assertEquals(checkoutPage.getCheckoutInfo(), "Your information");
    }
    @Test(description = " missing first name for delivery an error message appears when user click on Checkout button")
    @Description ("if the first name is missing for delivery then an error message appears")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop",url ="https://fasttrackit-test.netlify.app/#/")
    @Story("delivery name is not completed then  error message appears when click on checkout button")
    public void missing_first_name_for_delivery_error_message_appears_when_click_on_Checkout_button() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        checkoutPage.clickOnTheContinueCheckoutButton();
        assertFalse(checkoutPage.getMissingFirstNameMsg(), "First Name is required");
    }
    @Test(description = " missing last name for delivery an error message appears when user click on Checkout button")
    @Description ("if the last name is missing for delivery then an error message appears")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop",url ="https://fasttrackit-test.netlify.app/#/")
    @Story("delivery name is not completed then  error message appears when click on checkout button")
    public void missing_last_name_for_delivery_error_message_appears_when_click_on_Checkout_button() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        checkoutPage.typeInFirstname(" Sherlock");
        checkoutPage.clickOnTheContinueCheckoutButton();
        assertFalse(checkoutPage.getMissingLastNameMsg(),"Last Name is required");
    }

    @Test(description = "missing delivery address an error message appears when user click on Checkout button")
    @Description ("if the delivery address is then an error message appears")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop",url ="https://fasttrackit-test.netlify.app/#/")
    @Story("delivery address is not completed then  error message appears when click on checkout button")
    public void missing_delivery_address_error_message_appears_when_click_on_Checkout_button() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        checkoutPage.typeInFirstname(" Sherlock");
        checkoutPage.typeInLastname(" Holmes");
        checkoutPage.clickOnTheContinueCheckoutButton();
        assertFalse(checkoutPage.getMissingAddressMsg(),"Address is required");
    }
}

