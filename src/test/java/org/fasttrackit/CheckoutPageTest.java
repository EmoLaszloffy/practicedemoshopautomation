package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

@Epic("Checkout Page Tests  ")
@Feature(" User can navigate to Checkout page and perform operations")
public class CheckoutPageTest {
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    CheckoutPage checkoutPage = new CheckoutPage();
    OrderSummaryPage orderSummaryPage = new OrderSummaryPage();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickOnTheResetButton();
        header.clickOnTheShoppingBagIcon();
    }

    @Test(description = "adding one product to cart page user can click on Checkout button")
    @Description(" after adding one product to cart user can go to checkout page ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("user can navigate to Checkout page by clicking to checout button")
    public void adding_one_product_to_cart_page_user_can_click_on_Checkout_button(){
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        assertEquals(checkoutPage.getCheckoutInfo(),"Your information");
    }
    @Test(description = "adding one product to cart page user can click on the Cancel button")
    @Description("user can click on Cancel button from cart page after adding one product to cart  ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("user can add one product to cart and click on cancel button from cart page")
    public void from_cart_page_user_can_click_on_Cancel_Button_and_navigate_to_Cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        checkoutPage.clickOnTheCancelButton();
        assertEquals(cartPage.getYourCartText(), "Your cart");
    }

    @Test(description = "after completing delivery information user can continue with checkout ")
    @Description("adding one product to cart and completing delivery information user can continue with Checkout  ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("completing delivery information user can continue with Checkout")
    public void adding_one_product_to_cart_user_continue_continue_with_Checkout() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        checkoutPage.typeInFirstname(" Mary");
        checkoutPage.typeInLastname(" Poppins");
        checkoutPage.typeInAddress(" Somewhere over the Rainbow");
        checkoutPage.typeInEmail("marry@mail.com");
        checkoutPage.clickOnTheContinueCheckoutButton();
        assertEquals(orderSummaryPage.getOrderSummaryInfo(), "Order summary");
    }
    @Test(description = "user can navigate to Home Page from Checkout page ")
    @Description("User can navigate to home page from checkout page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("navigation to Home page from Checkout")
    public void user_can_navigate_to_Home_page_from_Checkout_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Products page.");
    }

}

