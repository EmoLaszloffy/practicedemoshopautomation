package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
@Epic("Cart tests from main page")
@Feature(" Main page cart interaction tests")

public class MainPageCartTest {
    Page page = new Page();
    Header header = new Header();
    @BeforeClass
    public void setup(){
        page.openHomePage();
    }
    @AfterMethod
    public void cleanup(){
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickOnTheResetButton();
    }

    @Test(description = "user can navigate to Cart page")
    @Description("user can navigate to Cart page from Home page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("cart page navigation from home page")
    public void user_can_navigate_to_Cart_Page(){
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(),"Your cart","expected to be on the Cart page.");
    }

    @Test(description = "user can navigate to Home page from Cart page")
    @Description("user can navigate to Home page from Cart page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("navigation to home page from cart page")
    public void user_can_navigate_to_Home_page_from_Cart_Page(){
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(),"Products","Expected to be on Products page");
    }

    @Test(description = "user can add one product to cart from product cart")
    @Description(" adding one product to cart from product cart")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("user can add product from product cart to cart")
    public void user_can_add_one_product_to_cart_from_product_cart(){
        Product product = new Product("5");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart,badge shows 1");
    }
    @Test(description = "user can duplicate de amount of one product in cart from product cart")
    @Description(" increase the amount of product in cart from product cart")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("user can duplicate the amount of a product in cart fom product cart")
    public void user_can_duplicate_the_amount_of_products_in_cart_from_product_cart() {
        Product product = new Product("4");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two product to cart,badge shows 2");
    }
    @Test(description = "user can add two different products to cart from product cart")
    @Description(" adding two different products to cart from product cart")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("user can add two different products from product cart to cart")
    public void user_can_add_two_different_products_to_product_cart(){
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        Product product1 = new Product("2");
        product1.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two different products to cart,badge shows 2");

    }

}