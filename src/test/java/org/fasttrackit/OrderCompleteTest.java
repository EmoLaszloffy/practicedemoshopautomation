package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
@Epic("Order Complete test")
@Feature(" User can navigate to order complete page")
public class OrderCompleteTest {
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    CheckoutPage checkoutPage = new CheckoutPage();
    OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
    OrderCompletePage orderCompletePage = new OrderCompletePage();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickOnTheResetButton();
        header.clickOnTheShoppingBagIcon();
    }

    @Test(description = "adding one product to cart and completing delivery information user can complete the order")
    @Description("adding one product to cart and completing delivery information user complete the order  ")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("completing order ")
    public void adding_one_product_to_cart_user_can_complete_the_order() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        checkoutPage.typeInFirstname(" Mary");
        checkoutPage.typeInLastname(" Poppins");
        checkoutPage.typeInAddress(" Somewhere over the Rainbow");
        checkoutPage.typeInEmail("mary@mail.com");
        checkoutPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.getOrderSummaryInfo();
        orderSummaryPage.clickOnTheCompleteYourOrderButton();
        assertEquals(orderCompletePage.getOrderComplete(),"Order complete","Thank you for your order!");
    }
    @Test(description = "user can click on the Cancel Button from order summary page")
    @Description("user can click on the cancel button from the order summary page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("from order summary page user can click on the cancel button")
    public void user_can_click_on_the_cancel_button_from_order_summary_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        checkoutPage.typeInFirstname(" Mary");
        checkoutPage.typeInLastname(" Poppins");
        checkoutPage.typeInAddress(" Somewhere over the Rainbow");
        checkoutPage.typeInEmail("mary@mail.com");
        checkoutPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.getOrderSummaryInfo();
        orderSummaryPage.clickOnTheCancelButton();
        assertEquals(cartPage.getYourCartText(), "Your cart","Expected to navigate back to Cart Page");
    }
    @Test(description = "user can click on Continue Shopping button from Order Complete page")
    @Description("After complete the order user can click on the Continue Shopping button")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("from order summary page user can click on the Continue Shopping button")
    public void after_complete_the_order_user_can_click_on_continue_shopping_button() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        checkoutPage.typeInFirstname(" Mary");
        checkoutPage.typeInLastname(" Poppins");
        checkoutPage.typeInAddress(" Somewhere over the Rainbow");
        checkoutPage.typeInEmail("mary@mail.com");
        checkoutPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.getOrderSummaryInfo();
        orderSummaryPage.clickOnTheCompleteYourOrderButton();
        orderCompletePage.getOrderComplete();
        orderCompletePage.clickOnTheContinueShoppingButton();
        assertEquals(page.getPageTitle(), "Products", "Expected to navigate to Products page");
    }
}
