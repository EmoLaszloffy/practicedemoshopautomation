package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class SearchTest {
    Page page = new Page();
    ProductCards productList = new ProductCards();
    @BeforeClass
    public void setup(){
        page.openHomePage();
    }
    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickOnTheResetButton();
    }

    @Test(description = "user can search products using search bar")
    @Description("user can search products using searchbar")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("Search bar test")
    public void user_can_search_products_using_search_bar(){
        productList.clickOnSearchField();
        productList.typeInSearchContent("pizza");
        productList.clickOnTheSearchButton();
        Assert.assertFalse(productList.getNumberOfSearchResults(),"Get search content");
    }
}
