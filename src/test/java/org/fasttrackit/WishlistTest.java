package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Epic("Wishlist management tests")
@Feature("User can perform operations on Wishlist Page")
public class WishlistTest {
    Page page = new Page();
    Header header = new Header();
    @BeforeClass
    public void setup(){
        page.openHomePage();
    }
    @AfterMethod
    public void cleanup(){
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickOnTheResetButton();
    }
    @Test(description = "user can navigate to wishlist page")
    @Description("user can navigate to Wishlist from Home page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("wishlist page navigation from home page")
    public void user_can_navigate_to_Wishlist_Page(){
        header.clickOnTheWishlistIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on the Wishlist page.");
    }

    @Test(description = "user can navigate to Home page from Wishlist page")
    @Description("user can navigate to Home page from Wishlist ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("home page navigation from Wishlist page")
    public void user_can_navigate_to_Home_Page_from_Wishlist_Page(){
        header.clickOnTheWishlistIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Products page.");
    }
    @Test(description = "user can add one product to Wishlist from Product cart")
    @Description("user can add one product from product cart to wishlist")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("adding one product to Wishlist from product cart")
    public void user_can_add_one_product_to_wishlist_from_product_cart(){
        Product product = new Product("5");
        product.clickOnTheWishlistHartIcon();
        assertTrue(header.isWishlistBadgeVisible());
        String badgeValue = header.getWishlistBadgeValue();
        assertEquals(badgeValue, "1", "After adding one products to wishlist, badge shows 1");
    }

    @Test(description = "user can add two products to Wishlist from Product cart")
    @Description("user can add two products from product cart to wishlist")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("adding two products to Wishlist from product cart")
    public void user_can_add_two_product_to_wishlist_from_product_cart(){
        Product product = new Product("5");
        product.clickOnTheWishlistHartIcon();
        Product product1 = new Product("1");
        product1.clickOnTheWishlistHartIcon();
        assertTrue(header.isWishlistBadgeVisible());
        String badgeValue = header.getWishlistBadgeValue();
        assertEquals(badgeValue, "2", "After adding two product to cart,badge shows 2");
    }
     @Test(description = "user can delete one product from wishlist after adding to products")
     @Description("user delete one product from wishlist")
     @Severity(SeverityLevel.NORMAL)
     @Owner("Emoke Laszloffi")
     @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
     @Issue("DMS - 001")
     @Story("adding two products to Wishlist user can delete one")
    public void user_can_delete_one_product_from_product_cart_after_adding_two_products_to_wishlist(){
        Product product = new Product("5");
        product.clickOnTheWishlistHartIcon();
        Product product1 = new Product("1");
        product1.clickOnTheWishlistHartIcon();
        assertTrue(header.isWishlistBadgeVisible());
        product.clickOnRemoveFromWishlistIcon();
        String badgeValue = header.getWishlistBadgeValue();
        assertEquals(badgeValue, "1", "After deleting one product from wishlist cart badge shows 1");
    }

}
