package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
@Test( groups = "Authentication Test")
@Epic("Authentication tests")

public class AuthenticationTest {
    Page page = new Page();
    Header header = new Header();
    ModalPage modal = new ModalPage();
    @BeforeClass
    public void setup(){
        page.openHomePage();
    }
    @AfterMethod
    public void cleanup(){
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickOnTheResetButton();
    }
    @Feature(("Login"))
    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class )
    @Description(" valid User can log in with valid credentials.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-int.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("Login with valid Credentials")
    public void valid_user_can_login_with_valid_credentials(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        assertEquals(header.getWelcomeMessage(), account.getWelcomeMessage(), "Logged in with valid credentials expected greetings message to be:" + account.getWelcomeMessage());
    }

    @Feature("Logout")
    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class )
    @Description(" User can logout after successful login.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("Logout after successful login with valid Credentials")
    public void valid_user_can_logout_after_login_with_valid_credentials(ValidAccount account)  {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        header.clickOnTheLogoutButton();
        assertEquals(header.getWelcomeMessage(), "Hello guest!", "Logout, expected greetings message to be Hello guest!");

    }

    @Feature("Error message")
    @Test(dataProvider = "invalidCredentials",dataProviderClass = AuthenticationDataProvider.class )
    @Description(" User clocked cannot login")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("User is locked out")
    public void invalid_credentials_user_cannot_login(InvalidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        boolean errorMsgDisplayed = modal.getErrorMsgDisplayed();
        String errorMsg = modal.getErrorMsg();
        assertTrue(errorMsgDisplayed, "Error message is displayed");
        assertEquals(errorMsg,account.getErrorMsg());
    }

}
