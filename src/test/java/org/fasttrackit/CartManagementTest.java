package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

@Epic("Cart management tests  ")
@Feature(" User can perform operations in cart page")
public class CartManagementTest {
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickOnTheResetButton();
        header.clickOnTheShoppingBagIcon();
    }

    @Test(description = "when user navigate to cart page an empty cart page is shown")
    @Description(" User can navigate to cart page and an empty cart page is displayed ")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("navigation to empty cart page  ")
    public void when_user_navigate_to_cart_page_empty_cart_page_is_displayed() {
        header.clickOnTheCartIcon();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test(description = "adding one product to cart empty cart page is not displayed")
    @Description(" by adding one product to cart empty cart page message is not shown ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("user can add one product to cart")
    public void adding_one_product_to_cart_page_empty_cart_page_is_not_displayed() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed(), " after adding one product to cart,cart page shows the added product");
    }

    @Test(description = "user can add two different products to cart ")
    @Description(" by adding two different products to cart empty cart page message is not shown ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("user can add two different products to cart")
    public void user_can_add_two_different_products_to_cart_page_empty_cart_page_is_not_displayed() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        Product product1 = new Product("1");
        product1.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed(), "After adding two different product to cart,cart page shows the 2 added product");
    }

    @Test(description = "user can increment the amount of a product in cart page")
    @Description(" user can increase the amount of a product ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("user can increase the amount of a product in cart")
    public void user_can_increment_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("9");
        item.increaseAmount();
        assertEquals(item.getItemAmount(), "2");
    }

    @Test(description = "user can reduce the amount of a product in cart page")
    @Description(" user can decrease the amount of a poruduct  ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("user can decrease the amount of a product in cart")
    public void user_can_reduce_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("9");
        item.reduceAmount();
        assertEquals(item.getItemAmount(), "1");
    }

    @Test(description = "user can delete the product form cart page from trash button")
    @Description(" user can delete the product from cart page clicking on trash button  ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("product from cart page can be deleted from trash button")
    public void user_can_delete_the_product_in_cart_page_from_trash_button() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("9");
        item.deleteItem();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test(description = "adding one product to cart page user can click on the Continue Shopping button")
    @Description(" user can click to the Continue shopping button from cart page ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS - 001")
    @Story("user can click to the Continue Shopping button from Cart page if adding one product to cart")
    public void adding_one_product_to_cart_page_user_can_click_on_Continue_Shopping_button(){
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheContinueShoppingButton();
        assertEquals(page.getPageTitle(), "Products");
    }
}
