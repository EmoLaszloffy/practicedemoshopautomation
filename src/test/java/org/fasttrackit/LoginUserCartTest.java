package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Epic("Login user Cart Interaction test")
@Feature("Login user can navigate trough demo shop webpage ")

public class LoginUserCartTest {
    Page page = new Page();
    Header header = new Header();
    ModalPage modal = new ModalPage();
    CartPage cartPage = new CartPage();
    CheckoutPage checkoutPage = new CheckoutPage();
    OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
    OrderCompletePage orderCompletePage = new OrderCompletePage();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickOnTheResetButton();
    }
    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("if login user dino can add one product to cart")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("valid user  can add one product to cart")
    public void valid_user_can_add_one_product_to_cart(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed(), " after adding one product to cart,cart page shows the added product");
    }
    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("valid user can add one product to cart from product cart ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("valid user can add one product to cart from product cart")
    public void valid_user_can_add_one_product_to_cart_from_product_cart(ValidAccount account){
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        Product product = new Product("5");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart,badge shows 1");
    }
    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("if login valid user can add two products to cart from product cart ")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("valid user can add two products to cart from product cart")
    public void valid_user_can_add_two_different_products_to_cart_from_product_cart(ValidAccount account){
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        Product product = new Product("5");
        product.clickOnTheProductCartIcon();
        Product product1 = new Product("2");
        product1.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two product to cart,badge shows 2");
    }


    @Test(dataProvider = "validCredentials" , dataProviderClass = AuthenticationDataProvider.class)
    @Description("valid user can add one product to cart and complete the order process ")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Emoke Laszloffi")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("valid user can add one product to cart from product cart and complete the order process successfully")
    public void valid_use_can_add_one_product_to_cart_and_complete_order(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.getWelcomeMessage();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.getCheckoutInfo();
        checkoutPage.typeInFirstname(" Mary");
        checkoutPage.typeInLastname(" Poppins");
        checkoutPage.typeInAddress(" Somewhere over the Rainbow");
        checkoutPage.typeInEmail("marry@mail.com");
        checkoutPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.getOrderSummaryInfo();
        orderSummaryPage.clickOnTheCompleteYourOrderButton();
        assertEquals(orderCompletePage.getOrderComplete(),"Order complete","Thank you for your order!");

    }
}

