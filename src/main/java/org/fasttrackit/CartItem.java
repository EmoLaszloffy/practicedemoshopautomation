package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.elements;

public class CartItem {

    private final SelenideElement row;
    private final SelenideElement reduceAmount;
    private final SelenideElement increaseAmount;
    private final SelenideElement amountControlSection;
    private final SelenideElement deleteItem;

    public CartItem(String productID) {
        String itemById = String.format("item_%s_title_link", productID);
        SelenideElement item = $(By.id(itemById));
        this.row = item.parent().parent();
        this.reduceAmount =row.$(".fa-minus-circle");
        this.increaseAmount =row.$(".fa-plus-circle");
        this.amountControlSection = this.increaseAmount.parent().parent();
        this.deleteItem = row.$(".fa-trash");
    }

    @Step("Increase Amount")
    public void increaseAmount(){
        this.increaseAmount.click();
    }

    @Step("Reduce Amount ")
    public void reduceAmount(){
        this.reduceAmount.click();
    }
    public String getItemAmount(){
        return this.amountControlSection.text();
    }
    @Step("Delete Item")
    public void deleteItem(){
        this.deleteItem.click();
    }
}
