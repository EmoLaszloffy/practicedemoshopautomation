package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class ModalPage {
    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".modal-dialog .fa-sign-in-alt");
    private final SelenideElement errorElement = $(".error");

  //  private final SelenideElement close = $(".close");

    @Step("Type in Username")
    public void typeInUsername(String user) {
        System.out.println("Click ont the Username field.");
        username.click();
        System.out.println("Type in " + user);
        username.type(user);
    }
    @Step("Type in password")
    public void typeInPassword(String pass) {
        System.out.println("Click on the password field. ");
        password.click();
        System.out.println("type in " + pass);
        password.type(pass);
    }
    @Step("Click on the Login Button")
    public void clickOnTheLoginButton() {
        System.out.println("Click on the Login Button");
        loginButton.click();
    }

    public boolean getErrorMsgDisplayed(){
        return this.errorElement.isDisplayed();
    }

    public String getErrorMsg(){
        return this.errorElement.text();
    }
    //  @Step("Click on the close button")
    //   public void clickOnTheCloseButton(){
    //   close.click();
    //   System.out.println("Click on the close button");
    //   }

}





