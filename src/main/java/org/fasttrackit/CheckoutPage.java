package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutPage {

    private final SelenideElement checkoutInfo = $(".subheader-container .text-muted");

    private final SelenideElement firstName = $("#first-name");
    private final SelenideElement lastName = $("#last-name");
    private final SelenideElement address = $("#address");

    private final SelenideElement email = $("#email");
    private final SelenideElement continueCheckout = $(".btn-success");
    private final SelenideElement cancel = $(".btn-danger");
    private final SelenideElement firstNameISRequired = $(".error");
    private final SelenideElement lastNameIsRequired = $(".error");
    private final SelenideElement addressIsRequired = $(".error");

    public CheckoutPage() {
    }

    public String getCheckoutInfo(){
        return checkoutInfo.text();
    }

    @Step("Introduce First name")
    public void typeInFirstname(String firstname){
        System.out.println("click on the Firstname field");
        firstName.click();
        System.out.println("type in: " + firstname);
        firstName.type(firstname);
    }
    @Step("Introduce Last name")
    public void typeInLastname(String lastname){
        System.out.println("click on the Lastname field");
        lastName.click();
        System.out.println("type in: " + lastname);
        lastName.type(lastname);
    }
    @Step("Introduce Address")
    public void typeInAddress(String Address){
        System.out.println("click on the Address field");
        address.click();
        System.out.println("type in: " + Address);
        address.type(Address);
    }

    @Step("Introduce email")
    public void typeInEmail (String eMail){
        System.out.println("click on the Email field");
        email.click();
        System.out.println("type in: " + eMail);
        email.type(eMail);
    }
    @Step("Click on the Continue Checkout Button")
    public void clickOnTheContinueCheckoutButton(){
        continueCheckout.click();
        System.out.println("Click on the Continue Checkout Button");
    }

    @Step("Click on the Cancel Button")
    public void clickOnTheCancelButton(){
        cancel.click();
        System.out.println("Click on the Cancel Button");
    }

    public boolean getMissingFirstNameMsg(){
        return !this.firstNameISRequired.isDisplayed();
    }
    public boolean getMissingLastNameMsg(){return !this.lastNameIsRequired.isDisplayed();}
    public boolean getMissingAddressMsg(){return !this.addressIsRequired.isDisplayed();}

}
