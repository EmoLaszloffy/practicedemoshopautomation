package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class OrderCompletePage {

    private final SelenideElement orderComplete = $(".subheader-container .text-muted");
    private final SelenideElement continueShopping = $(".btn-success");

    public OrderCompletePage(){
    }
    public String getOrderComplete(){
        return orderComplete.text();
    }
    @Step("Click on the Continue Shopping Button")
    public void clickOnTheContinueShoppingButton(){
        continueShopping.click();
        System.out.println("Click on the Continue Shopping button");
    }
}
