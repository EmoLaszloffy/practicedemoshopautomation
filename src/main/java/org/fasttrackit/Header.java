package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement logoutButton = $(".navbar .fa-sign-out-alt");
    private final SelenideElement wishlistButton = $(".navbar .fa-heart");
    private final SelenideElement welcomeElement = $(".navbar-text span span");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final ElementsCollection shoppingCartBadges =  $$(".shopping_cart_badge");
    private final SelenideElement wishlistBadge = $(".fa-hart,.shopping_cart_badge");
    private final ElementsCollection wishlistBadges = $$(".fa-hart,.shopping_cart_badge");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");

    @Step("Click on the Sign In button")
    public void clickOnTheLoginButton(){
        loginButton.click();
        System.out.println("Click on the SignIn button");
    }
    public String getWelcomeMessage(){
        return welcomeElement.text();
    }

    @Step("Click on the Sign Out button")
    public void clickOnTheLogoutButton() {
        logoutButton.click();
        System.out.println("Click on the SignOut button");
    }
    @Step("Click on the Wishlist button")
    public void clickOnTheWishlistIcon() {
        System.out.println("Click on the Wishlist button");
        wishlistButton.click();
    }

    @Step("Click on the Shopping Bag Icon ")
    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping Bag icon");
        homePageButton.click();
    }
    @Step("Click on the Shopping Cart Icon")
    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart Icon");
        cartIcon.click();
    }

    public String getShoppingCartBadgeValue(){
        return this.shoppingCartBadge.text();
    }
    public String getWishlistBadgeValue(){
        return  this.wishlistBadge.text();
    }

    public boolean isShoppingBadgeVisible (){
        return !this.shoppingCartBadges.isEmpty();
    }
    public boolean isWishlistBadgeVisible(){
        return !this.wishlistBadges.isEmpty();
    }

}
