package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class OrderSummaryPage {

    private final SelenideElement orderSummary = $(".subheader-container .text-muted");
    private final SelenideElement completeYourOrder = $(".btn-success");

    private final SelenideElement cancel = $(".btn-danger");
    public OrderSummaryPage(){
    }
    public String getOrderSummaryInfo(){
        return orderSummary.text();
    }
    @Step("Click on the complete your order button")
    public void clickOnTheCompleteYourOrderButton(){
        completeYourOrder.click();
        System.out.println("Click on the Complete Your Order button");
    }
    @Step ("Click on the Cancel Button")
    public void clickOnTheCancelButton(){
        cancel.click();
        System.out.println("Click on the Cancel Button");
    }
}
