package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {

    private final SelenideElement emptyCartPageElement = $(".text-center");
    private final SelenideElement checkoutButton = $(".btn-success");
    private final SelenideElement continueShoppingButton = $(".btn-danger");
    private final SelenideElement yourCart = $(".subheader-container .text-muted");

    public CartPage() {
    }

    public String getEmptyCartPageText(){
        return this.emptyCartPageElement.text();
    }
    public boolean isEmptyCartMessageDisplayed(){
        return emptyCartPageElement.isDisplayed();
    }

    @Step("Click on the Checkout Button")
    public void clickOnTheCheckoutButton(){
        checkoutButton.click();
        System.out.println("Click on the Checkout Button");
    }
    @Step("Click on the Continue Shopping Button")
    public void clickOnTheContinueShoppingButton(){
        continueShoppingButton.click();
        System.out.println("Click on the Continue Shopping Button");
    }

    public String getYourCartText(){
        return this.yourCart.text();
    }
}
