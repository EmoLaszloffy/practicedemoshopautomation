package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Product {

    private final SelenideElement card;
    private final SelenideElement addToCardButton;

    private final SelenideElement addToWishlistButton;

    private final SelenideElement removeFromWishlistButton;

    private  final String title;

    private final String price;

    public Product(SelenideElement card) {
        this.card = card;
        this.addToCardButton = card.$(".card-footer .fa-cart-plus");
        this.addToWishlistButton = card.$(".card-footer .fa-heart");
        this.removeFromWishlistButton = card.$(".card-footer .fa-heart-broken");
        this.title = this.card.$(".card-link").text();
        this.price = this.card.$(".card-footer .card-text span").text();
    }
    public Product(String productId) {
        String productIdSelector = String.format("[href='#/product/%s']",productId);
        SelenideElement cardLink = $(productIdSelector);
        this.card = cardLink.parent().parent();
        this.addToCardButton = card.$(".card-footer .fa-cart-plus");
        this.addToWishlistButton = card.$(".card-footer .fa-heart");
        this.removeFromWishlistButton = card.$(".card-footer .fa-heart-broken");
        this.title = cardLink.text();
        this.price = this.card.$(".card-footer .card-text span").text();
    }

    public String getTitle() {
        return title;
    }

    public String getPrice(){
        return price;
    }

    @Step("Click on the add to Cart Icon")
    public void clickOnTheProductCartIcon() {
        System.out.println("Click on the add to Cart Icon");
        this.addToCardButton.click();
    }
    @Step("Click on the Hart Icon")
    public void clickOnTheWishlistHartIcon(){
        System.out.println("Click on the Hart Icon");
        this.addToWishlistButton.click();
    }
    @Step("Click on the broken hart icon")
    public void clickOnRemoveFromWishlistIcon(){
        System.out.println("Click on the broken hart icon");
        this.removeFromWishlistButton.click();
    }
}
