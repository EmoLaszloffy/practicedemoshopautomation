package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ProductCards {
    private final ElementsCollection cards = $$(".card");
    private final SelenideElement sortButton = $(".sort-products-select");
    private final SelenideElement sortAZ = $("option[value=az]");
    private final SelenideElement sortZA = $("option[value=za]");
    private final SelenideElement sortLoHi = $("option[value=lohi]");
    private final SelenideElement sortHiLo = $("option[value=hilo]");
    private final SelenideElement search = $("#input-search");
    private final SelenideElement searchButton = $(".btn-light");
    private final SelenideElement searchResults = $("a[class='card-link']");
    public ProductCards() {
    }
    @Step("click on the Sort Button")
    public void clickOnTheSortButton() {
        this.sortButton.click();
    }
    @Step("click on the AZ Sort Button")
    public void clickOnTheAZSortButton() {
        this.sortAZ.click();
    }
    @Step("click on the ZA Sort Button")
    public void clickOnTheZASortButton() {
        this.sortZA.click();
    }
    @Step("click on the Sort Price Low High ")
    public void clickOnTheSortByPriceLoHi() {
        this.sortLoHi.click();
    }
    @Step("click on the Sort Price High Low")
    public void clickOnTheSortByPriceHiLo() {
        this.sortHiLo.click();
    }



    public Product getFirstProductInList() {
        SelenideElement first = cards.first();
        return new Product(first);
    }

    public Product getLastProductInList() {
        SelenideElement last = cards.last();
        return new Product(last);
    }

    @Step("Click on the search field")
    public void clickOnSearchField(){
        System.out.println("click on the search field");
        search.click();
    }
    @Step
    public void typeInSearchContent(String Content) {
        System.out.println("type in: " + Content);
        search.type(Content);
    }

    @Step("Click on the Search Button")
    public void clickOnTheSearchButton() {
        this.searchButton.click();
    }
    public boolean getNumberOfSearchResults() {
        return !this.searchResults.isDisplayed();
    }


}